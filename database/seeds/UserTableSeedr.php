<?php

use Illuminate\Database\Seeder;

class UserTableSeedr extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->setAttribute('password', 'password');
        $user->setAttribute('phone_number', '9841145614');
        $user->save();
        $user->role()->save((new \App\UserRole())->setAttribute('role_id',1));
        $user = new \App\User();
        $user->setAttribute('password', 'password');
        $user->setAttribute('phone_number', '9841145615');
        $user->save();
        $user->role()->save((new \App\UserRole())->setAttribute('role_id',2));
        $user = new \App\User();
        $user->setAttribute('password', 'password');
        $user->setAttribute('phone_number', '9841145616');
        $user->save();
        $user->role()->save((new \App\UserRole())->setAttribute('role_id',3));

    }
}
