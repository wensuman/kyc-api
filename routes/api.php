<?php

ApiRoute::post('auth/login', 'LoginController@handle');

ApiRoute::post('auth/verify', 'RegisterController@verify');

ApiRoute::any('auth/register', 'RegisterController@handle');

ApiRoute::get('bank-list', 'BankController@lists');

ApiRoute::post('forgot-password', 'PasswordController@forgot');

ApiRoute::post('upload', 'UploadController@upload');

ApiRoute::group(['middleware' => 'jwt.auth'], function () {

    ApiRoute::get('log', 'LogController@respond');

    ApiRoute::group(['middleware' => 'user'], function () {

        ApiRoute::get('dashboard', 'DashboardController@dashboard');

        ApiRoute::post('change-password', 'PasswordController@change');

        ApiRoute::get('access/{form_id}', 'AccessController@lists');

        ApiRoute::get('template', 'TemplateController@index');

        ApiRoute::post('template', 'TemplateController@store');

        ApiRoute::post('submission/{id?}', 'SubmissionController@store');

        ApiRoute::get('history/{id?}', 'HistoryController@index');

        ApiRoute::get('submission/{id?}', 'HistoryController@index');

        ApiRoute::delete('history/{id}', 'HistoryController@destroy');

        ApiRoute::get('profile/me', 'ProfileController@show');

        ApiRoute::post('profile/me', 'ProfileController@update');

        ApiRoute::get('tickets', 'TicketController@index');

        ApiRoute::post('tickets', 'TicketController@store');


    });

});

ApiRoute::get('voters-details','ProfileController@voters');
