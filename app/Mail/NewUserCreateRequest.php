<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserCreateRequest extends Mailable
{
    use  SerializesModels;
    public $pin;
    public $emailed;
    public $subject = 'Verify your new account at KYC NEPAL';

    /**
     * Create a new message instance.
     *
     * @param $pin
     * @param $email
     */
    public function __construct($pin, $email)
    {
        $this->pin = $pin;
        $this->emailed = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->markdown('mail.new-user-request', ['pin' => $this->pin, 'email' => $this->emailed, 'url' => env('APPI_URL')]);
        return $this;
    }
}
