<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormSubmitted extends Mailable
{
    use SerializesModels;

    public $for;

    public $subject = "Update on your KYC Form";

    /**
     * Create a new message instance.
     *
     * @param $to
     * @internal param $array
     */
    public function __construct($to)
    {
        $this->for = $to;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->markdown('mail.form', ['bank_name' => $this->for]);
    }
}
