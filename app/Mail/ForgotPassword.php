<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use  SerializesModels;

    public $password;
    public $email;

    public $subject = 'Your new password for KYC NEPAL';

    /**
     * Create a new message instance.
     * @param $password
     * @param $email
     */
    public function __construct($password, $email)
    {

        $this->password = $password;

        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->markdown('mail.forgot-password',['password'=>$this->password,'email'=>$this->email,'url'=>env('APPI_URL')]);
    }
}
