<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormSubmittedByUser extends Mailable
{
    use  SerializesModels;

    public $array;

    public $link;

    public $subject = 'A new KYC Form to verify';

    /**
     * Create a new message instance.
     *
     * @param $link
     * @internal param $array
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.formby', ['url' => $this->link]);
    }
}
