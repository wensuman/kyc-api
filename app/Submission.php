<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Submission extends Model
{
    use SoftDeletes,Notifiable;


    public $hidden = ['submissions','updated_at','user_id','deleted_at','bank_id'];

    public $casts = [
        'submissions'=>'json'
    ];

    /**
     * One submission belongs to one bank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank(){
        return $this->belongsTo(User::class,'bank_id');
    }

     /**
     * One submission belongs to one bank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

}
