<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubmission;
use App\Submission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Notifications\NewSubmissionMade;
use App\Mail\FormSubmitted;
use App\Mail\FormSubmittedByUser;
use App\User;
use Cache;
use App\SMS;
use Mail;

class SubmissionController extends Controller
{
    /**
     * Store a form submitted to bank.
     *
     * @param StoreSubmission|Request $request
     * @param null $id
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubmission $request, $id = null)
    {
        $submission = Submission::where('bank_id', $request->get('bank_id'))->where('user_id', $request->user()->id)->first();
        if ($submission && !$id) {
            /**
             * If submission already exists we will have to check one thing.
             */
            if ($submission->verification_requested_at || $submission->verified_at) {
                /**
                 * If the verification was already requested for submission or is already verified, we will reject the request.
                 */
                $this->response->errorBadRequest();
            }
        }

        $submission = $submission ?? new Submission();
        $submission->setAttribute('bank_id', $request->get('bank_id'));
        $submission->setAttribute('user_id', $request->user()->id);
        $submission->setAttribute('submissions', $request->get('data'));
        $submission->setAttribute('verification_requested_at', Carbon::now());
        $submission->setAttribute('rejected_at', null);
        $submission->setAttribute('verified_at', null);
        $submission->save();
        /**
         * Retrieve the bank
         */
        $bank = User::roleBank()->where('id', $request->get('bank_id'))->first();
        /**
         * Notify the bank and user with mail and SMS
         */
        $this->notify($request, $bank, $submission);

        activity('submission')->causedBy($request->user())->on($submission)->withProperties(['ip' => $request->ip(), 'data' => $request->all(), 'browser' => $_SERVER])->log("Form was successfully submitted");

        return $this->response->created();
    }

    /**
     * @param StoreSubmission $request
     * @param $bank
     * @param $submission
     * @internal param $bank_name
     */
    public function notify(StoreSubmission $request, $bank, $submission)
    {


        $text = $bank->profile['name'] ?? 'a bank ';

        /**
         * Send SMS if mobile number exists
         */
        request()->user()->phone_number && app(SMS::class)->to(request()->user()->phone_number)->text('Your form has been submitted to ' . $text . ' at KYC NEPAL.')->send();

        /**
         * Send email is email address exists
         */
        request()->user()->email && @Mail::to(request()->user())->send(new FormSubmitted($text));

        /**
         * Send emails to bank informing the user submitted a form at their bank
         */
        Mail::to($bank)->send(new FormSubmittedByUser(env('BANK_URL') . 'submissions/' . $submission->id));

        /**
         * Find the bank and send the submission request
         */
        User::find($request->get('bank_id'))->notify(new NewSubmissionMade($submission));
    }
}
