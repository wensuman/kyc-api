<?php

namespace App\Http\Controllers;

use App\Transformers\SubmissionTransformer;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * Display a listing of the forms submitted.
     *
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null)
    {
        $with = $request->user()->submissions()->with('bank');

        if ($id) {
            /**
             * If ID exists, return the respective submisssion
             */

            $data = $with->findOrFail($id);

            $data->setAttribute('core', true);

            return $this->response->item($data, new SubmissionTransformer());
        }
        /**
         * If ID does not exists return all the submission
         */

        return $this->response->collection($with->get(), new SubmissionTransformer);
    }
}
