<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTemplate;
use App\Mail\VerificationSubmitted;
use App\SMS;
use App\Template;
use App\Transformers\TemplateTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;

class TemplateController extends Controller
{
    /**
     * Return the template of the user.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->item($request->user()->template ?? new Template(), new TemplateTransformer);
    }

    /**
     * Save the template. This process is atomic. All are saved, none are updated.
     *
     * @param StoreTemplate|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTemplate $request)
    {
        /**
         * Retrieve a template or create a new one
         */
        $template = $request->user()->template()->first() ?: new Template;

        /**
         * Set user_id
         */
        $template->setAttribute('user_id', $request->user()->id);

        /**
         * Set the date
         */
        $template->template = $request->get('data');

        /**
         * Determine if request is for verification
         */
        if ($request->has('is_verify_request')) {
            /**
             * If request is for verification
             */
            $template->rejected_at = null;

            $template->verification_requested_at = Carbon::now();

        }
        /**
         * Save the template
         */
        $template->save();

        /**
         * Lof the activity
         */
        activity('template')->causedBy($request->user())->on($template)->withProperties(['ip' => $request->ip(), 'data' => $request->all(), 'browser' => $_SERVER])->log('Form was successfully saved');

        /**
         * Notify necessary users about verify request
         */
        $request->has('is_verify_request') && $this->notify($template);

        return $this->response->accepted();
    }

    /**
     * @param $template
     */
    public function notify($template)
    {
        /**
         * Send SMS if phone number exists
         */
        request()->user()->phone_number && app(SMS::class)->to(request()->user()->phone_number)->text('Your form has been submitted for verification at KYC NEPAL. ')->send();

        /**
         * Send emails to admins
         */
        User::roleAdmin()->get()->each->notify(new \App\Notifications\NewTemplateVerificationRequest($template));

        /**
         * Send email to user saying request was submitted
         */
        request()->user()->email && Mail::to(request()->user())->send(new VerificationSubmitted());
    }


}
