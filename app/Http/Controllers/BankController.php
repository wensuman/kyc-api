<?php

namespace App\Http\Controllers;

use App\Transformers\BankTransformer;
use App\User;

class BankController extends Controller
{
	/**
	 * Get the list of banks registered in KYC Nepal
	 **/
    public function lists(){
        return $this->response->collection(User::roleBank()->get(), new  BankTransformer);
    }
}
