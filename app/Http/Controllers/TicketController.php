<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transformers\TicketTransformer;
use App\Ticket;

class TicketController extends Controller
{
    /**
     * Return the list of tickets owned by the user
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->paginator($request->user()->tickets()->latest()->paginate(5), new TicketTransformer);
    }

    /**
     * Save the ticket for the user
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Validate the ticket
         */
        $this->validate($request, ['title' => 'required', 'detail' => 'required'], ['detail.required' => 'Question detail is required', 'title.required' => 'Title is required.']);

        /**
         * Save the ticker
         */
        $request->user()->tickets()->save(app(Ticket::class)->setAttribute('bank_id', 0)->setAttribute('ticket', $request->only('title', 'detail')));

        /**
         * Log the activity for the ticket
         */
        activity('ticket')->by($request->user())->withProperties(['ip' => $request->ip(), 'browser' => $_SERVER])->log("A new ticket was created");

        return $this->response->created();
    }
}
