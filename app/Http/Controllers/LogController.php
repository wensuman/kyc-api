<?php

namespace App\Http\Controllers;


use App\Transformers\LogTransformer;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class LogController extends Controller
{
    /**
     * Return the list of log rows of specific users
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function respond(Request $request){

        return $this->response->paginator(Activity::where('causer_id',$request->user()->id)->orderByDesc('created_at')->take(10)->paginate(), new LogTransformer);
    }
}
