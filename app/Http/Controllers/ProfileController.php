<?php

namespace App\Http\Controllers;

use App\Transformers\ProfileTransformer;
use App\Transformers\VotersTransformer;
use App\Voter;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Return the profile detail of the user
     **/
    public function show()
    {
        return $this->response->item(auth()->user(), new ProfileTransformer);
    }

    /**
     * Return new voters API
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function voters(Request $request)
    {
        return $this->response->item(Voter::where('application_no', $request->get('voters_id'))->firstOrFail(), new VotersTransformer());
    }
}