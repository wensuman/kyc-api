<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePassword;
use App\Http\Requests\ForgotPassword;
use App\SMS;
use App\User;
use Mail;
use Cache;

class PasswordController extends Controller
{
    /**
     * Change the password of the user
     * @param ChangePassword $request
     * @return \Dingo\Api\Http\Response
     */
    public function change(ChangePassword $request)
    {
        /**
         * Try to attempt the auth first
         */
        if (auth()->attempt(['phone_number' => $request->user()->phone_number, 'password' => $request->get('old_password')])) {
            /**
             * If successfull, change the password.
             */
            auth()->user()->setAttribute('password', $request->get('new_password'))->save();

            return $this->response->accepted();
        }
        /**
         * Otherwuse it is error
         */
        $this->response->errorUnauthorized();
    }

    /**
     * Fire an sms for user who has forgotten the password
     * @param ForgotPassword $request
     * @return \Dingo\Api\Http\Response
     */
    public function forgot(ForgotPassword $request)
    {

        /**
         * Retrieve the user based on string given
         */
        $user = User::roleUser()->where(str_contains($request->get('phone_number'), '@') ? 'email' : 'phone_number', $request->get('phone_number'))->firstOrFail();

        /**
         * We do not want somebody constantly requesting for password change.
         * So, we will keep the information on last request,
         * using this key.
         */
        $key = "{$request->get('phone_number')}_forgot";

        /**
         * Using transaction because, we do not want to be failed and not send information in email or SMS.
         */
        \DB::transaction(function () use ($user, $request, $key) {

            /**
             * Generate the random string of length 8
             */
            $password = str_random(8);

            /**
             * Update the user now.
             */
            $user->setAttribute('password', $password);

            $user->save();

            /**
             * We will send an sms for this user notifying new password.
             */
            $user->phone_number && app(SMS::class)->to($user->phone_number)->text("Your new password for KYC NEPAL is {$password}")->send();

            /**
             * If the user has email, we will also send an email for the new password.
             */
            $user->email && Mail::to($user)->send(new \App\Mail\ForgotPassword($password, $user->email));

            /**
             * Since we have sent mail and number, we will put information in cache.
             * It will help us to identify that, we just sent the sms.
             */
            Cache::put($key, 'yes', 30);

        });

        return $this->response->accepted();
    }
}
