<?php

namespace App\Http\Controllers;

use App\Transformers\UploadTransformer;
use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;
class UploadController extends Controller
{
    /**
     * A single handle api that takes any file uploaded and returns the file
     * @param UploadRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function upload(UploadRequest $request){
        return $this->response->collection(collect($request->allFiles()), new UploadTransformer);
    }
}
