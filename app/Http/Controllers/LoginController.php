<?php

namespace App\Http\Controllers;

use App\History;
use App\Http\Requests\SignInRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class LoginController extends Controller
{

    /**
     * Perform the sign in of the user;
     * @param SignInRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function handle(SignInRequest $request)
    {
        /**
         * This login is only applicable for normal users.
         */
        $roleUser = User::roleUser();

        /**
         * Password field will be common among authentication
         */
        $field = ['password' => $request->get('password')];
        /**
         * Get the identifier, in here phone number is the one.
         */
        $identifier = strtolower($request->get('phone_number'));
        /**
         * If the identifier contains @, then it must be treated as email otherwise it will be treated as phone number
         */
        $str_contains = str_contains($identifier, '@') ? 'email' : 'phone_number';
        /**
         * User may be doing login either with email or phone number, so we will guess which ever fits
         */
        $field[$str_contains] = $identifier;

        /**
         * Retrieve the matching record from database
         */
        $user = $roleUser->where($str_contains, $identifier)->first();
        /**
         * Convert the authentication detail to token
         */
        $token = JWTAuth::attempt($field);

        if ($token && $user) {
            /**
             * If attempt was valid and token was generate, log the acctivity and return the token as response.
             */
            activity('login')->by($request->user())->withProperties(['ip' => $request->ip(), 'browser' => $_SERVER])->log('Login successful');

            return response()->json(['data' => ['token' => $token]])->cookie('Authorization', 'Bearer ' . $token, 3000)->setStatusCode(202);

        }
        if ($user) {
            /**
             * Else we will report that an attempt with invalid password was done.
             */
            activity('login')->by(User::where('phone_number', $identifier)->first())->withProperties(['ip' => $request->ip(), 'browser' => $_SERVER])->log('Login was denied');

        }
        /**
         * Return unauthorized
         */
        return $this->response->errorUnauthorized();
    }

}
