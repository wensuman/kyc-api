<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignUpRequest;
use App\Mail\NewUserCreateRequest;
use App\SMS;
use App\User;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Cache;
use Mail;

class RegisterController extends Controller
{
    /**
     * Register a new user. Put it in cache for time being. Only store when it is verified.
     * @param SignUpRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function handle(SignUpRequest $request)
    {
        /**
         * Generate the random pin
         */
        $pin = random_int(1000, 9999);
        /**
         * If the user has entered a phone number
         */
        if ($request->get('phone_number')) {
            /**
             * Let the pin be valid for one hour
             */
            Cache::put($request->get('phone_number'), $pin, Carbon::now()->addHour());
            /**
             * If the user input the phone number, then we store a cache with all the query parameters that has one hour of expiry date.
             */
            Cache::put($request->get('phone_number') . '_data', $request->all(), Carbon::now()->addHour());

            /**
             * Send SMS now
             */
            app(SMS::class)->to($request->get('phone_number'))->text("Your verification code for KYC NEPAL is {$pin}.")->send();

        }

        /**
         * If the user has entered the email
         */
        if ($request->get('email', false)) {
            /**
             * Let the pin be valid for one hour
             */
            Cache::put($request->get('email'), $pin, Carbon::now()->addHour());
            /**
             * We store a cache with all the query parameters that has one hour of expiry date.
             */
            Cache::put($request->get('email') . '_data', $request->all(), Carbon::now()->addHour());
            /**
             * Send am email for the user
             */
            Mail::to(app(\App\User::class)->setAttribute('email', $request->get('email')))->send(new NewUserCreateRequest($pin, $request->get('email')));
        }

        return $this->response->created();

    }

    /**
     * Verify the user
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function verify(Request $request)
    {
        return $this->sign($request);
    }

    /**
     * Save the user in database, so that they can login now
     *
     * @param $request
     * @return \Dingo\Api\Http\Response
     */
    public function sign(Request $request)
    {
        if (!Cache::has($request->get('phone_number'))) {

            $this->response->errorBadRequest();

        }

        $code = Cache::get($request->get('phone_number'));

        if ($code != $request->get('verification_code')) {
            $this->response->errorBadRequest();
        }

        \DB::transaction(function () use ($request) {

            $user = new User();

            /**
             * Retrieve the data from cache.
             */
            $cache = Cache::get($request->get('phone_number') . '_data');

            /**
             * Set phone number is exists.
             */
            ($cache['phone_number'] ?? false) && $user->setAttribute('phone_number', $cache['phone_number'] ?? null);

            /**
             * Set email if exists.
             */
            ($cache['email'] ?? false) && $user->setAttribute('email', $cache['email'] ?? null);

            /**
             * Set the password, will be encrypted by default wit bcrypt.
             */
            $user->setAttribute('password', $cache['password']);

            /**
             * Save the user.
             */
            $user->save();

            /**
             * Save the role.
             */
            $user->role()->save(app(UserRole::class)->setAttribute('role_id', 1));

            /**
             * Log the activitys
             */
            activity('register')->by($user)->withProperties(['ip' => $request->ip(), 'browser' => $_SERVER])->log('Sign Up successful');
            /**
             * Remove the cache
             */
            Cache::forget($request->get('phone_number'));

        });


        return $this->response->created();
    }

}
