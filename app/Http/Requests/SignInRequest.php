<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;

class SignInRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $column = str_contains($this->get('phone_number'), '@') ? 'email' : 'phone_number';
        return [
            'phone_number' => 'required|exists:users,' . $column,
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'phone_number.required' => 'The field phone number is required',
            'phone_number.exists' => 'Login Unauthorized',
            'password.required' => 'The field password is required',
        ];
    }
}
