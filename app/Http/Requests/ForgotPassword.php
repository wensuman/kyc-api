<?php

namespace App\Http\Requests;


use Dingo\Api\Http\FormRequest;

class ForgotPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $var = str_contains($this->get('phone_number'), '@') ? 'email' : 'phone_number';
        return [
            'phone_number' => 'required|exists:users,' .$var
        ];
    }

    /**
     * Return the appropriate message for the error occured in the request
     *
     **/
    public function message()
    {
        $constraint = str_contains($this->get('phone_number'), '@') ? 'email' : 'mobile number';
        return [
            'phone_number.exists' => 'Entered ' . $constraint . ' has not been used in this website.'
        ];
    }
}
