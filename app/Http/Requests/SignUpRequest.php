<?php

namespace App\Http\Requests;


use Dingo\Api\Http\FormRequest;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => 'digits:10|unique:users,phone_number',
            'email' => 'unique:users,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
            'register_agree' => 'in:1'
        ];
    }

    public function messages()
    {
        return [
            'phone_number.unique' => 'The phone number is already registered.',
            'email.unique' => 'The email address is already registered.',
            'register_agree' => 'Please, agree with out terms and contitions.'
        ];
    }
}
