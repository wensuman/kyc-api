<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public $casts=[
    	'ticket'=>'json'
    ];

    /**
     * One ticket belongs to one bank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank(){
    	return $this->belongsTo(User::class,'bank_id');
    }

    /**
     * One ticket belongs to one user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
    	return $this->belongsTo(User::class,'user_id');
    }
}
