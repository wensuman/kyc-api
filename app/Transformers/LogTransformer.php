<?php
namespace  App\Transformers;

use League\Fractal\TransformerAbstract;
use Spatie\Activitylog\Models\Activity;

class LogTransformer extends TransformerAbstract{
    /**
     * @param Activity $activity
     * @return array
     */
    public function transform(Activity $activity){
        return ['description'=>$activity->description,'properties'=>$activity->properties,'log_name'=>$activity->log_name,'created_at'=>$activity->created_at->toFormattedDateString()];
    }

}