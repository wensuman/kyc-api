<?php
namespace  App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract{
    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user){
        return array_only($user->toArray(), ['id','email','phone_number','hashed_id']);
    }

}