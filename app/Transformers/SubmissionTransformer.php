<?php
namespace  App\Transformers;

use App\Submission;
use League\Fractal\TransformerAbstract;

class SubmissionTransformer extends TransformerAbstract{
    /**
     * @param Submission $submission
     * @return array
     */
    public function transform(Submission $submission){
        return array_merge(array_filter(['verification_requested_at'=>$submission->verification_requested_at,'verified_at'=>$submission->verified_at,'rejected_at'=>$submission->rejected_at]), $submission->submissions);
    }

}