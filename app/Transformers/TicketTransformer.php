<?php
namespace  App\Transformers;

use App\Ticket;
use League\Fractal\TransformerAbstract;

class TicketTransformer extends TransformerAbstract{

    /**
     * @param Ticket $ticket
     * @return array
     */
    public function transform(Ticket $ticket){
        return array_only($ticket->toArray(),['id','ticket','created_at','deleted_at']);
    }

}