<?php

namespace App\Transformers;

use App\Voter;
use League\Fractal\TransformerAbstract;

class VotersTransformer extends TransformerAbstract
{
    /**
     * @param Voter $voter
     * @return array
     * @internal param UploadedFile $file
     */
    public function transform(Voter $voter)
    {
        return $voter->getAttributes();
    }

}