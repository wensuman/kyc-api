<?php

namespace App\Transformers;

use App\Template;
use League\Fractal\TransformerAbstract;

class TemplateTransformer extends TransformerAbstract
{
    /**
     * @param Template $template
     * @return array
     */
    public function transform(Template $template)
    {
        $template->voters_data = @$template && @$template->template['voters_id_no'] ? \App\Voter::find($template->template['voters_id_no']) ?? [] : [];

        return array_filter(array_merge(['type' => 'template', 'verification_requested_at' => $template->verification_requested_at ? $template->verification_requested_at->toDateTimeString() : null, 'verified_at' => $template->verified_at ? $template->verified_at->toDateTimeString() : null, 'rejected_at' => $template->rejected_at ? $template->rejected_at->toDateTimeString() : null], array_except((array)$template->getAttribute('template'), ['verification_requested_at', 'rejected_at', 'verified_at'])));
    }

}