<?php

namespace App\Transformers;

use Illuminate\Http\UploadedFile;
use League\Fractal\TransformerAbstract;
use Spatie\Activitylog\Models\Activity;

class UploadTransformer extends TransformerAbstract
{
    /**
     * @param UploadedFile $file
     * @return array
     */
    public function transform(UploadedFile $file)
    {
        return ['name' => $file->storePubliclyAs('uploads',$file->getClientOriginalName().'--'.str_random(8).'.'.$file->getClientOriginalExtension())];
    }
}