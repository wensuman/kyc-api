<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\SMS;
use Cache;

class NewTemplateVerificationRequest extends Notification
{
 public $template;

    /**
     * Create a new notification instance.
     *
     * @param $template
     */
    public function __construct($template)
    {
        $this->template = $template;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['kyc-administrators-log-report'];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
       return new BroadcastMessage(['message'=>"A Customer submitted an form verification request.",'link'=>'/template/'.$this->template->id]);
    }
    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array|BroadcastMessage
     */
    public function toArray($notifiable)
    {
        return ['message'=>"A customer submitted an form verification request.",'link'=>'/template/'.$this->template->id];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $customer = $this->template->template['applicant_name'] ?? '';
        return ['message'=>"A customer submitted an form verification request.",'link'=>'/template/'.$this->template->id];
    }
}
