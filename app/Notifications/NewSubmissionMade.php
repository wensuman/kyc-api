<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\SMS;

class NewSubmissionMade extends Notification
{

    public $submission;

    /**
     * Create a new notification instance.
     *
     * @param $submission
     */
    public function __construct($submission)
    {
        $this->submission = $submission;
    }

     /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['kyc-bank-'.$this->submission->bank_id];
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(['message'=>"A customer submitted an kyc application to your bank.",'link'=>'/submissions/'.$this->submission->id]);
    }
      /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array|BroadcastMessage
       */
    public function toArray($notifiable)
    {
        return ['message'=>"A customer submitted an kyc application to your bank.",'link'=>'/submissions/'.$this->submission->id];
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $customer = $this->submission->submissions['applicant_name'] ?? 'A customer';
          return ['message'=>"{$customer} submitted an kyc application to your bank.",'link'=>'/submissions/'.$this->submission->id];
    }
}
