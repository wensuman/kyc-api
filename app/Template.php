<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    use SoftDeletes;

    public $casts = [
        'template' => 'json'
    ];

    public $dates = [
        'verification_requested_at', 'verified_at', 'rejected_at'
    ];

    /**
     * One template belongs to one user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
