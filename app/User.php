<?php

namespace App;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    public $appends = ['hashed_id'];

    public function getHashedIdAttribute()
    {
        return substr(strtoupper(preg_replace('/\d+/u', '', md5($this->id))), 0,3).'-'.$this->id;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public $casts = [
        'profile' => 'json',
        'screening' => 'json'
    ];

    /**
     * Encrypt the password
     * @param $attribute
     * @return $this
     */
    public function setPasswordAttribute($attribute)
    {
        $this->attributes['password'] = bcrypt($attribute);
        return $this;
    }

    /**
     * Encrypt the password
     * @param $attribute
     * @return $this
     */
    public function setEmailAttribute($attribute)
    {

        $this->attributes['email'] = strtolower($attribute);

        return $this;
    }

    /**
     * One User can have more than one role.
     */
    public function role()
    {
        return $this->hasMany(UserRole::class);
    }

    /**
     * Get the history of the user for log purpose
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(History::class);
    }

    /**
     * Get the template that belongs to user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template()
    {
        return $this->hasOne(Template::class);
    }

    /**
     * Get the submissions of the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }

    /**
     * Determine if the user is bank
     * @return bool
     */
    public function isBank()
    {
        return auth()->check() && auth()->user()->role()->where('role_id', 2)->first();
    }

    /**
     * Determine if the user is admin
     * @return bool
     */
    public function isAdmin()
    {
        return auth()->check() && auth()->user()->role()->where('role_id', 3)->first();
    }

    /**
     * Determine if the user is customer or not
     * @return bool
     */
    public function isSimpleUser()
    {
        return auth()->check() && auth()->user()->role()->where('role_id', 1)->first();
    }

    /**
     * User having role of bank
     * @return mixed
     */
    public static function roleBank()
    {
        return self::wherehas('role', function ($query) {
            $query->where('role_id', '2');
        });
    }

    /**
     * User having role of admin
     * @return mixed
     */
    public static function roleAdmin()
    {
        return self::wherehas('role', function ($query) {
            $query->where('role_id', '3');
        });
    }

    /**
     * Users having role of normal user.
     * @return mixed
     */
    public static function roleUser()
    {
        return self::wherehas('role', function ($query) {
            $query->where('role_id', '1');
        });
    }

    /**
     * One user has many tickets
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * One user may have many tickets
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function issues()
    {
        return $this->hasMany(Ticket::class, 'bank_id');
    }

    /**
     * This particular user received broadcast notification here.
     * @return array
     */
    public function receivesBroadcastNotificationsOn()
    {
        return [
            new PrivateChannel('users.' . $this->id),
        ];
    }

}
