<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Client;

class SMS extends Model
{
    /**
     * Set the phone number of reciever
     * @param $to
     * @return $this
     */
    public function to($to)
    {
        $this->setAttribute('from', 'WebSMS');
        $this->setAttribute('token', '8iHt7JoSXCPnqmuHX21Y');
        $this->setAttribute('to', $to);
        return $this;
    }

    /**
     * Set the text to send sms
     * @param $text
     * @return $this
     */
    public function text($text)
    {
        $this->setAttribute('text', $text.'%0aThank You,%0a KYC NEPAL');
        $this->text = $text;
        return $this;
    }

    /**
     * Send the SMS
     * @return bool|string
     */
    public function send()
    {
        if (env('SMS_ENABLED', false))
            return file_get_contents('http://api.sparrowsms.com/v2/sms?' . http_build_query($this->getAttributes()));
    }

}
