<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
    public $primaryKey = 'application_no';

    public $timestamps = false;

    public $hidden = ['district','tole','municipality','ward_no'];

    public $appends  = ['permanent_district','permanent_tole','permanent_municipality','permanent_ward_no'];

    public function getPermanentDistrictAttribute()
    {
        return $this->district;
    }

    public function getPermanentToleAttribute()
    {
        return $this->tole;
    }

    public function getPermanentMunicipalityAttribute()
    {
        return $this->municipality;
    }

    public function getPermanentWardNoAttribute()
    {
        return $this->ward_no;
    }
}
