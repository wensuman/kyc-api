<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '1090260407786584',
        'client_secret' => 'aaca00dd71f2df54f96fbd3b39c1ef37',
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],
    'twitter' => [
        'client_id' => 'x6O3FRK2i5RwOt2luxySGPXVW',
        'client_secret' => 'lGiqN1Lg5dOQwgq0atdodKlONmIprmHsI2L8kfbG9eAL52x8Wf',
        'redirect' => 'http://localhost:8000/login/twitter/callback',
    ],
    'google'=>[
        'client_id' => '402832968225-s9btq5g9o9q1s3beg5taj2svp9m6omhm.apps.googleusercontent.com',
        'client_secret' => 'dactEBOdJ2DoW40XxOLEkv4S',
        'redirect' => 'http://localhost:8000/login/google/callback',
    ]

];
