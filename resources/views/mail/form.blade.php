@component('mail::message')
Your KYC form has been successfully submitted to {{$bank_name}}.
<br/>
Thank you,<br/>
<a href="http://kycnepal.com.np" style="text-decoration: none;">KYC NEPAL</a>
@endcomponent