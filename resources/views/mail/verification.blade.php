@component('mail::message')
Your KYC form has been submitted for verification at KYC NEPAL.
<br/>
Thank you,<br/>
<a href="http://kycnepal.com.np"  style="text-decoration: none;">KYC NEPAL</a>
@endcomponent