@component('mail::message')
Your verification code for KYC NEPAL registration is
<br>
<b>{{$pin}}</b>
<br/>
<a href="http://{{$url}}/verify?email={{$email}}">Click here</a> to verify your account.
<br/>
Thank you,<br/>
<a href="http://kycnepal.com.np" style="text-decoration: none;">KYC NEPAL</a>
@endcomponent