@component('mail::message')
# You recently requested for a new password at KYC NEPAL.
<br>
Your new password for KYC NEPAL is <span style="background-color: #f0f8ff">{{$password}}</span> .

@component('mail::button', ['url' => $url])
    Log In
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent