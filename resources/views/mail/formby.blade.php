@component('mail::message')
A new KYC form has been submitted to your bank.<br/> Click here <a href="{{$url}}">here</a> to view it.
<br/>
Thank you,<br/>
<a href="http://kycnepal.com.np" style="text-decoration: none;">KYC NEPAL</a>
@endcomponent